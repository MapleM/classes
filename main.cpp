#include <iostream>

#include "Demo.h"

using namespace std;

int main(){
	Investment inv = Investment(0.083,100.00);
	int counter = 0;
	
	while(inv.getVal() < 1000000)
	{
		for(int i = 0; i < 12; i++)
		{
			inv.setVal(inv.getVal() + 250);
		}
		inv.addIntrest();
		counter++;
	}
	
	cout << "It would take " << counter << " years to become a millionare." << endl;

	return 0;
}
