#ifndef Demo.h
#define Demo.h
#endif

class Investment 
 {
	private:  //properties
         long double value;
         float interest_rate; 
         


	public:  //methods
        
		Investment();//default constructor 
		Investment(float,double);
		void setVal(double);
		float getVal();
		void addIntrest();

 };

