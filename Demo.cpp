#include "Demo.h"

Investment::Investment()
{
	value = 0;
	interest_rate = 0; 
}

Investment::Investment(float intrest, double val)
{
	value = val;
	interest_rate = intrest; 
}

void Investment::setVal(double val)
{
	value = val;
}

float Investment::getVal()
{
	return value;
}

void Investment::addIntrest()
{
	value = value + (value * interest_rate);
}
